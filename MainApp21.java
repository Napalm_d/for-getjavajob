//9.42. Составить программу, которая печатает заданное слово, начиная с последней буквы.

import java.util.Scanner;

public class MainApp21 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите слово: ");
        String word = sc.next();

        StringBuilder builder = new StringBuilder(word);
        System.out.println("Зеркальное отображение слова: " + builder.reverse());
    }
}