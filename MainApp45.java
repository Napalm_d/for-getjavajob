//12.234. Дан двумерный массив.
// а) Удалить из него k-ю строку.
// б) Удалить из него s-й столбец.

import java.util.Random;
import java.util.Scanner;

public class MainApp45 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите k: ");
        int k = sc.nextInt();
        System.out.print("Введите s: ");
        int s = sc.nextInt();

        Random rd = new Random();
        int[][] arr = new int[10][5];

        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                arr[i][j] = rd.nextInt(20) + 1;
            }
        }
        
        System.out.println("Исходный массив: ");
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println("");
        }
        ArrModifier am = new ArrModifier();

        am.lineRemove(arr, k);
        System.out.println("Массив после удаления " + k + " строки: ");
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println("");
        }
        am.colRemove(arr, s);
        System.out.println("Массив после удаления " + s + " столбца: ");
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                System.out.print(arr[i][j] + " ");
            }
            System.out.println("");
        }
    }
}
class ArrModifier {
    // метод для удаления k-й строки
    public int[][] lineRemove (int[][] arr, int k) {
        for (int i = k; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                arr[i-1][j] = arr[i][j];
            }
        }
        for (int i = arr.length-1; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                arr[i][j] = 0;
            }
        }
        return arr;
    }
    // метод для удаления s-го столбца
    public int[][] colRemove (int[][] arr, int s) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = s; j < arr[0].length; j++) {
                arr[i][j-1] = arr[i][j];
            }
        }
        for (int i = 0; i < arr.length; i++) {
            for (int j = arr[0].length - 1; j < arr[0].length; j++) {
                arr[i][j] = 0;
            }
        }
        return arr;
    }
}