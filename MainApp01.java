//1.3.  Составить программу вывода на экран числа, вводимого с клавиатуры.
//      Выводимому числу должно предшествовать сообщение "Вы ввели число".


import java.util.Scanner;

public class MainApp01 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in); // Создаем объект Scanner для ввода с клавиатуры
        int i = scanner.nextInt(); // Вводим число с клавиатуры
        System.out.println("Вы ввели число " + i); // Вывод строки и введенного числа

    }
}
