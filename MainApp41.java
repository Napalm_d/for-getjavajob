//11.245.*Дан массив. Переписать его элементы в другой массив такого же размера следующим образом:
// сначала должны идти все отрицательные элементы, а затем все остальные.
// Использовать только один проход по исходному массиву.

import java.util.Arrays;

public class MainApp41 {
    public static void main(String[] args) {
        int[] startarray = {1, -5, 6, 2, -4, -7, 7, -9, 3, 4};
        int[] finalarray = new int[10];
        int temp = 0;
        int temp2 = startarray.length - 1;

        // отрицательные элементы кладем с начала нового массива, все остальные - с конца
        for (int i = 0; i < startarray.length; i++) {
            if (startarray[i] < 0) {
                finalarray[temp] = startarray[i];
                temp++;
            }
            else {
                finalarray[temp2] = startarray[i];
                temp2--;
            }
        }
        System.out.println("Исходный массив: " + Arrays.toString(startarray));
        System.out.println("Обработанный массив: " + Arrays.toString(finalarray));
    }
}