// 12.23. Заполнить двумерный массив размером 7x7 так, как показано на рис. 12.1.

import java.util.Arrays;
import java.util.Scanner;

public class MainApp42 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите размерность массива: ");
        int n = sc.nextInt();

        int[][] arrayA = new int[n][n];
        int[][] arrayB = new int[n][n];
        int[][] arrayC = new int[n][n];
        ArrayModifier am = new ArrayModifier();

        System.out.println("Массив А: ");
        am.arrayFillA(arrayA, n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arrayA[i][j] + " ");
            }
            System.out.println("");
        }
        am.arrayAtest(arrayA); // тест на equals

        System.out.println("Массив B: ");
        am.arrayFillB(arrayB, n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arrayB[i][j] + " ");
            }
            System.out.println("");
        }
        am.arrayBtest(arrayB);

        System.out.println("Массив C: ");
        am.arrayFillC(arrayC, n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arrayC[i][j] + " ");
            }
            System.out.println("");
        }
        am.arrayCtest(arrayC);
    }
}
class ArrayModifier {
        public int[][] arrayFillA(int[][] arr, int n) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (i == j) {
                        arr[i][j] = 1;
                    } else if ((i + j) == (n - 1)) {
                        arr[i][j] = 1;
                    }
                }
            }
            return arr;
        }

        public void arrayAtest (int[][] arr) {
            int[][] arrayAtest = new int[][]{
                    {1, 0, 0, 0, 0, 0, 1},
                    {0, 1, 0, 0, 0, 1, 0},
                    {0, 0, 1, 0, 1, 0, 0},
                    {0, 0, 0, 1, 0, 0, 0},
                    {0, 0, 1, 0, 1, 0, 0},
                    {0, 1, 0, 0, 0, 1, 0},
                    {1, 0, 0, 0, 0, 0, 1}
            };
            if (Arrays.deepEquals(arr, arrayAtest)) {
                System.out.println("Test passed");
            } else System.out.println("Test not passed");
        }

        public int[][] arrayFillB(int[][] arr, int n) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (i == j) {
                        arr[i][j] = 1;
                    } else if ((i + j) == (n - 1)) {
                        arr[i][j] = 1;
                    } else if (i == n / 2 | j == n / 2) {
                        arr[i][j] = 1;
                    }
                }
            }
            return arr;
        }

    public void arrayBtest (int[][] arr) {
        int[][] arrayBtest = new int[][]{
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}
        };
        if (Arrays.deepEquals(arr, arrayBtest)) {
            System.out.println("Test passed");
        } else System.out.println("Test not passed");
    }

        public int[][] arrayFillC(int[][] arr, int n) {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    if (i <= (n / 2) & j >= i & (i + j) < n) {
                        arr[i][j] = 1;
                    }
                    else if (i > (n / 2) & (i + j) >= (n - 1) & j <= i) {
                        arr[i][j] = 1;
                    }
                }
            }
            return arr;
        }

    public void arrayCtest (int[][] arr) {
        int[][] arrayCtest = new int[][]{
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}
        };
        if (Arrays.deepEquals(arr, arrayCtest)) {
            System.out.println("Test passed");
        } else System.out.println("Test not passed");
    }
}