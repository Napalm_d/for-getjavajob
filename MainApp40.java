//11.158.*Удалить из массива все повторяющиеся элементы, оставив их первые вхождения,
// т. е. в массиве должны остаться только различные элементы.

import java.util.Arrays;


public class MainApp40 {
    public static void main(String[] args) {

        int[] arr = new int[] {1, 2, 3, 4, 5, 4, 3, 6, 7, 2}; // емкость - 10
        int[] newArr = new int[10];
        System.out.print("Массив: ");
        System.out.println(Arrays.toString(arr));

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[j] == arr[i]) {
                    arr[j] = 0;
                }
            }
        }
        int temp = 0; // счетчик для массива newArr
        for (int i = 0; i < arr.length; i++) {

            if (arr[i] != 0) {
                newArr[temp] = arr[i];
                temp++;
            }
        }

        System.out.print("Обработанный массив: ");
        System.out.println(Arrays.toString(newArr));
    }
}