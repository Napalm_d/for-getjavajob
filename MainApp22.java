//9.107. Дано слово. Поменять местами первую из букв а и последнюю из букв о.
// Учесть возможность того, что таких букв в слове может не быть.

import java.util.Scanner;

public class MainApp22 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите слово: ");
        String word = sc.next();

        char[] ch = word.toCharArray(); //преобразуем строку в массив символов
        int firstA = 0;
        int lastO = 0; // переменные, для хранения индексов искомых букв
        boolean hasA = false;
        boolean hasO = false;

        //проверяем, содержит ли слово буквы А и О
        for (int i = 0; i < ch.length; i++) {
            if (ch[i] == 'a') {
                hasA = true;
            } else if (ch[i] == 'o') {
                hasO = true;
            }
        }

        if (hasA && hasO) {
            //ищем индекс первой буквы А
            for (int i = 0; i < ch.length; i++) {
                if (ch[i] == 'a') {
                    firstA = i;
                    break;
                }
            }
            //ищем индекс последней буквы О
            for (int i = ch.length - 1; i > 0; i--) {
                if (ch[i] == 'o') {
                    lastO = i;
                    break;
                }
            }
            //меняем буквы местами
            char temp = ch[firstA];
            ch[firstA] = ch[lastO];
            ch[lastO] = temp;

            String newWord = new String(ch);
            System.out.println("Измененное слово: " + newWord);

        } else System.out.println("В слове нет буквы а или о");

    }
}