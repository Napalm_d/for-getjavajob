//9.22. Дано слово, состоящее из четного числа букв.
// Вывести на экран его первую половину, не используя оператор цикла.


import java.util.Scanner;

public class MainApp20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите слово с четным количеством букв: ");
        String word = sc.next();
        int lgth = word.length(); // вычисляем длину слова
        int halfLgth = lgth / 2; // первая половина слова
        word = word.substring(0, halfLgth); // обрезаем строку, отбрасываем вторую половину

        System.out.println("Первая половина слова: " + word);
    }
}