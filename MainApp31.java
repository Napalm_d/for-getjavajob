//10.48. Написать рекурсивную функцию для вычисления максимального элемента массива из n элементов.

import java.util.Arrays;
import java.util.Scanner;

public class MainApp31 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите количество элементов массива: ");
        int n = sc.nextInt(); // количество элементов массива
        int[] arr = new int[n]; // создаем массив из n элементов

        // заполняем массив рандомными значениями
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
        }

        System.out.println("Массив: " + Arrays.toString(arr));

        MathFunct mf = new MathFunct();
        System.out.println("Максимальный элемент: " + mf.maxValue(arr, n));

    }
}

class MathFunct {
    public int maxValue (int[] arr, int n) {
        int max = 0;
        if (n == 1) return arr[0];
        if (n > 1) {
            max = Math.max(arr[n-1], maxValue(arr, n-1));

        }
        return max;
    }

}