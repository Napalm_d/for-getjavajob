//1.17 o, p, r, s
//Записать по правилам изучаемого языка программирования следующие выражения:
//о) sqrt(1 - sin^2(x))
//p) 1 / sqrt(a * x^2 + b * x + c)
//r) (sqrt(x + 1) + sqrt(x - 1)) / (2 * sqrt(x))
//s) abs(x) + abs(x + 1)

import static java.lang.Math.*;


public class MainApp02 {
    public static void main(String[] args) {
        double a = 1; // переменная a с произвольным значением
        double b = 1; // переменная b с произвольным значением
        double c = 1; // переменная c с произвольным значением
        double x = 1; // переменная X с произвольным значением

        //o
        double result = sqrt(1 - Math.pow(sin(x), 2)); // итоговая запись выражения

        //p
        double result2 = 1 / sqrt(((a * Math.pow(x, 2)) + (b * x) + c )); // итоговая запись выражения

        //r
        double result3 = (sqrt(x + 1) + sqrt(x - 1)) / (2 * sqrt(x)); // итоговая запись выражения

        //s
        double result4 = abs(x) + abs(x + 1); // итоговая запись выражения
    }
}
