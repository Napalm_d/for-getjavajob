// 4.115. В некоторых странах Дальнего Востока (Китае, Японии и др.) использовался
// (и неофициально используется в настоящее время) календарь, отличающийся от применяемого нами.
// Этот календарь представляет собой 60-летнюю циклическую систему.
// Каждый 60-летний цикл состоит из пяти 12-летних подциклов. В каждом подцикле
// года носят названия животных: Крыса, Корова, Тигр, Заяц, Дракон, Змея, Лошадь, Овца,
// Обезьяна, Петух, Собака и Свинья. Кроме того, в названии года фигурируют цвета животных,
// которые связаны с пятью элементами природы — Деревом (зеленый), Огнем (красный), Землей (желтый),
// Металлом (белый) и Водой (черный). В результате каждое животное (и его год) имеет символический цвет,
// причем цвет этот часто совершенно не совпадает с его "естественной" окраской — Тигр может быть
// черным, Свинья — красной, а Лошадь — зеленой. Например, 1984 год — год начала очередного цикла —
// назывался годом Зеленой Крысы. Каждый цвет в цикле (начиная с зеленого) "действует" два года,
// поэтому через каждые 60 лет имя года (животное и его цвет) повторяется.
// Составить программу, которая по заданному номеру года нашей эры n печатает его название
// по описанному календарю в виде: "Крыса, Зеленый".
// Рассмотреть два случая:
// а) значение n >= 1984;
// б) значение n может быть любым натуральным числом.

import java.util.Scanner;

public class MainApp47 {
    public static void main(String[] args) {
        int start = 1; // отсчет с 1 года н.э.
        String animal = null;
        String color = null;

        Scanner sc = new Scanner(System.in);
        System.out.print("Введите год: ");
        int input = sc.nextInt();
        int value = (input - ((input - start) / 12) * 12) - start; // отбрасываем полные 12-летние циклы

        switch (value) {
            case 0:
                animal = "Cock";
                break;
            case 1:
                animal = "Dog";
                break;
            case 2:
                animal = "Pig";
                break;
            case 3:
                animal = "Rat";
                break;
            case 4:
                animal = "Cow";
                break;
            case 5:
                animal = "Tiger";
                break;
            case 6:
                animal = "Rabbit";
                break;
            case 7:
                animal = "Dragon";
                break;
            case 8:
                animal = "Snake";
                break;
            case 9:
                animal = "Horse";
                break;
            case 10:
                animal = "Sheep";
                break;
            case 11:
                animal = "Monkey";
                break;
        }

        int colorValue = (input - ((input - start) / 10) * 10) - start; // отбрасываем цветовые 10-летние циклы

        switch (colorValue) {
            case 0:
                color = "White";
                break;
            case 1:
                color = "Black";
                break;
            case 2:
                color = "Black";
                break;
            case 3:
                color = "Green";
                break;
            case 4:
                color = "Green";
                break;
            case 5:
                color = "Red";
                break;
            case 6:
                color = "Red";
                break;
            case 7:
                color = "Yellow";
                break;
            case 8:
                color = "Yellow";
                break;
            case 9:
                color = "White";
                break;
        }
        System.out.println(animal + ", " + color);
    }
}