//10.44. Написать рекурсивную функцию нахождения цифрового корня натурального числа.
//Цифровой корень данного числа получается следующим образом. Если сложить все цифры этого числа,
//затем все цифры найденной суммы и повторять этот процесс, то в результате будет получено однозначное число
//(цифра), которая и называется цифровым корнем данного числа.

import java.util.Scanner;

public class MainApp27 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число: ");
        int number = sc.nextInt();

        DigitalRad dr = new DigitalRad();
        System.out.println("Цифровой корень числа равен: " + dr.totalDigitalRad(number));
    }
}

class DigitalRad {

    public int digitalRad(int n) {
        if (n == 0) return 0;
        return n % 10 + digitalRad(n / 10);
    }

    public int totalDigitalRad(int n) {
        int result = digitalRad(n);
        if (result / 10 == 0) {
            return result;
        }
        return totalDigitalRad(n);
    }
}