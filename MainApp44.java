//12.63. В двумерном массиве хранится информация о количестве учеников в том
// или ином классе каждой параллели школы с первой по одиннадцатую
// (в первой строке — информация о количестве учеников в первых классах, во второй —
// о вторых и т. д.). В каждой параллели имеются 4 класса.
// Определить среднее количество учеников в классах каждой параллели.

import java.util.Arrays;
import java.util.Random;

public class MainApp44 {
    public static void main(String[] args) {
        Random rand = new Random();

        int[][] arr = new int[11][4];
        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 4; j++) {
                arr[i][j] = rand.nextInt(30) + 1;
            }
        }
        System.out.println("Исходный массив: ");
        System.out.println(Arrays.deepToString(arr));

        int[][] avgarr = new int[11][1];

        for (int i = 0; i < 11; i++) {
            for (int j = 0; j < 4; j++) {
                avgarr[i][0] = avgarr[i][0] + arr[i][j];
            }
        }

        for (int i = 0; i < 11; i++) {
            avgarr[i][0] = avgarr[i][0] / 4;                               
            }

        System.out.println("Массив со средними значениями: ");
        System.out.println(Arrays.deepToString(avgarr));
    }
}