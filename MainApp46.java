//13.12. Известна информация о 20 сотрудниках фирмы: фамилия, имя, отчество, адрес
// и дата поступления на работу (месяц, год).
// Напечатать фамилию, имя, отчество и адрес сотрудников, которые на сегодняшний день
// проработали в фирме не менее трех лет.
// День месяца не учитывать (при совпадении месяца поступления и месяца сегодняшнего дня считать,
// что прошел полный год).

import java.util.ArrayList;
import java.util.Scanner;

public class MainApp46 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите номер текущего месяца: ");
        int monthNow = sc.nextInt();
        System.out.print("Введите текущий год: ");
        int yearNow = sc.nextInt();

        Worker worker1 = new Worker("Krasnopolsky", "Aleksey", "Andreevich", "Moscow, Russia", 11, 2015);
        Worker worker2 = new Worker("Krasnopolsky", "Pavel", "Andreevich", "Moscow, Russia", 5, 2014);
        Worker worker3 = new Worker("Piven", "Dmitriy", "Stanislavovich", "Kiev, Ukraine", 6, 2012);
        Worker worker4 = new Worker("Krupnov", "Anton", "Aleksandrovich", "Saint-Petersburg, Russia", 9, 2013);
        Worker worker5 = new Worker("Dmitriev", "Aleksandr", "Mikhailovich", "Moscow, Russia", 4, 2017);
        Worker worker6 = new Worker("Ilyukhin", "Aleksandr", "Evgenevich", "Moscow, Russia", 10, 2014);
        Worker worker7 = new Worker("Isaev", "Sergey", "Gennadevich", "Moscow, Russia", 3, 2010);
        Worker worker8 = new Worker("Shevlyakov", "Mikhail", "Semenovich", "Moscow, Russia", 4, 2011);
        Worker worker9 = new Worker("Filippov", "Grigoriy", "Andreevich", "Moscow, Russia", 12, 2016);
        Worker worker10 = new Worker("Kashparov", "Ilya", "Valerevich", "Moscow, Russia", 6, 2014);
        Worker worker11 = new Worker("Nikolaev", "Aleksey", "Andreevich", "Moscow, Russia", 1, 2010);
        Worker worker12 = new Worker("Gorshkov", "Evgeniy", "Ilich", "Moscow, Russia", 8, 2011);
        Worker worker13 = new Worker("Korolko", "Viktoriya", "Dmitrievna", "Moscow, Russia", 7, 2012);
        Worker worker14 = new Worker("Gavrilova", "Elena", "Andreevna", "Moscow, Russia", 8, 2013);
        Worker worker15 = new Worker("Ratnikova", "Kseniya", "Alekseevna", "Moscow, Russia", 9, 2014);
        Worker worker16 = new Worker("Gorshkova", "Anastasiya", "Sergeevna", "Moscow, Russia", 10, 2015);
        Worker worker17 = new Worker("Chikirev", "Sergey", "Evgenevich", "Moscow, Russia", 11, 2016);
        Worker worker18 = new Worker("Byakov", "Yuriy", "Aleksandrovich", "Moscow, Russia", 12, 2017);
        Worker worker19 = new Worker("Klyuev", "Dmitriy", "Olegovich", "Kaliningrad, Russia", 5, 2013);
        Worker worker20 = new Worker("Gimatdinov", "Farid", "Sergeevich", "Kazan, Russia", 4, 2015);

        ArrayList<Worker> workersList = new ArrayList<>(); // создаем список работников

        Database db = new Database();
        // добавляем всех работников в список
        db.addToList(workersList, worker1);
        db.addToList(workersList, worker2);
        db.addToList(workersList, worker3);
        db.addToList(workersList, worker4);
        db.addToList(workersList, worker5);
        db.addToList(workersList, worker6);
        db.addToList(workersList, worker7);
        db.addToList(workersList, worker8);
        db.addToList(workersList, worker9);
        db.addToList(workersList, worker10);
        db.addToList(workersList, worker11);
        db.addToList(workersList, worker12);
        db.addToList(workersList, worker13);
        db.addToList(workersList, worker14);
        db.addToList(workersList, worker15);
        db.addToList(workersList, worker16);
        db.addToList(workersList, worker17);
        db.addToList(workersList, worker18);
        db.addToList(workersList, worker19);
        db.addToList(workersList, worker20);

        DateProcessor dp = new DateProcessor();
        
        // проверяем всех работников, тех кто работает 3 года и больше - выводим на экран
        for (Worker worker : workersList) {
            dp.worksForThreeYears(monthNow, yearNow, worker);
        }

    }
}

class Worker {
    private String lastname;
    private String firstname;
    private String secondname;
    private String address;
    private int hireMonth;
    private int hireYear;

    // основной конструктор
    public Worker (String lastname, String firstname, String secondname, String address, int hireMonth, int hireYear) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.secondname = secondname;
        this.address = address;
        this.hireMonth = hireMonth;
        this.hireYear = hireYear;
    }

    public String getWorker (Worker worker) {
        return worker.lastname + " " + worker.firstname + " " +
                worker.secondname + " | " + worker.address;
    }

    public String getLastname() {
        return this.lastname;
    }

    public String getFirstname() {
        return this.firstname;
    }

    public String getSecondname() {
        return this.secondname;
    }

    public String getAddress() {
        return this.address;
    }

    public int getHireMonth () {
        return  this.hireMonth;
    }

    public int getHireYear() {
        return this.hireYear;
    }
}
class Database {

    public ArrayList addToList (ArrayList list, Worker worker) {
        list.add(worker);
        return list;
    }
}
class DateProcessor {

    public void worksForThreeYears (int monthNow, int yearNow, Worker worker) {
        int yearDiff = yearNow - worker.getHireYear();
        int monthDiff = monthNow - worker.getHireMonth();
        if (yearDiff > 3) {
            System.out.println(worker.getLastname() + " " + worker.getFirstname() + " " + worker.getSecondname() + ", " + worker.getAddress());
        } else if (yearDiff == 3 && monthDiff >= 0 ) {
            System.out.println(worker.getLastname() + " " + worker.getFirstname() + " " + worker.getSecondname() + ", " + worker.getAddress());
        }
    }
}