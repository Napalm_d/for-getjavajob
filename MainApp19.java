//9.17. Дано слово. Верно ли, что оно начинается и оканчивается на одну и ту же букву?

import java.util.Scanner;

public class MainApp19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите слово: ");
        String word = sc.next();
        int lgth = word.length(); //вычисляем длину слова

        if (word.charAt(0) == word.charAt(lgth-1)) {
            System.out.println("Слово начинается и оканчивается на одну и ту же букву");
        } else System.out.println("Слово начинается и оканчивается на разные буквы");
    }
}