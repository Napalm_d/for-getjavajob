//4.67. Дано целое число k (1 <= k <= 365). Определить, каким будет k-й день года: выходным
//(суббота и воскресенье) или рабочим, если 1 января — понедельник.

public class MainApp11 {
    public static void main(String[] args) {
        DayQualifier.workOrWeekend(5);
        DayQualifier.workOrWeekend(7);
        DayQualifier.workOrWeekend(8);
        DayQualifier.workOrWeekend(20);
    }
}

class DayQualifier {

    public static void workOrWeekend(int day) {
        if (day > 7) {
            day = day - (day / 7) * 7; // отбрасываем полные недели, предшествующие введенному дню
        }
        if ( day >= 1 && day <=5) {
            System.out.println("workday");
        } else if (day > 5 && day <=7) {
            System.out.println("weekend");
        }
    }
}