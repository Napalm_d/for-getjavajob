//4.36. Работа светофора для пешеходов запрограммирована следующим образом: в
// начале каждого часа в течение трех минут горит зеленый сигнал, затем в течение
// двух минут — красный, в течение трех минут — опять зеленый и т. д.
// Дано вещественное число t, означающее время в минутах, прошедшее с начала
// очередного часа. Определить, сигнал какого цвета горит для пешеходов в этот момент.


import java.util.Scanner;

public class MainApp10 {
    public static void main(String[] args) {
        TrafficLight.color(3); //test1
        TrafficLight.color(5); //test2

    }
}

class TrafficLight {
    public static void color(int minutes) {
        if (minutes >= 5) {
            minutes = minutes - (minutes / 5) * 5; // отбрасываем полные циклы по (3 + 2) минут, если они есть
        }

        if (minutes < 3) {
            System.out.println("Green");
        } else if (minutes >= 3 && minutes < 5) {
            System.out.println("Red");
        }
    }
}