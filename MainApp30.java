//10.47. Написать рекурсивную функцию для вычисления k-го члена последовательности Фибоначчи.

import java.util.Scanner;

public class MainApp30 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите k: ");
        int k = sc.nextInt(); // к-й член последовательности Фибоначчи
        Rec rec = new Rec();
        System.out.println(k + "-й член последовательности Фибоначчи: " + rec.fibo(k));
    }
}
class Rec {
    public int fibo(int k) {
        if (k == 1) return 1;
        if (k == 2) return 1;
        int result = fibo(k - 1) + fibo(k - 2);
        return result;
    }
}