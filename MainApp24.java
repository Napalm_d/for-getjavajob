//10.41. Написать рекурсивную функцию для вычисления факториала натурального числа n.

import java.util.Scanner;

public class MainApp24 {
    public static void main(String[] args) {
        Factorial f = new Factorial();
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число, для которого необходимо вычислить факториал: ");
        int number = sc.nextInt();

        System.out.println("Факториал введенного числа равен " + f.fact(number));
    }
}

class Factorial {

    int fact(int n) {
        int result;

        if(n == 1) return 1;
        result = fact(n-1) * n;
        return result;
    }
}