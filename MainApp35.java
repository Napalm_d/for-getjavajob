// 10.52. Написать рекурсивную процедуру для вывода на экран цифр натурального числа в обратном порядке.

import java.util.Scanner;

public class MainApp35 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите чилос: ");
        int n = sc.nextInt();

        Task task = new Task();
        System.out.print("Введенное число в обратном порядке: ");
        task.reverse(n);
    }
}

class Task {
    public void reverse (int n) {
        if (n < 10) System.out.print(n);
        if (n >= 10) {
            System.out.print(n % 10);
            reverse(n / 10);
        }
    }
}