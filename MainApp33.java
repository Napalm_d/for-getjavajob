//10.50. Написать рекурсивную функцию для вычисления значения так называемой
// функции Аккермана для неотрицательных чисел n и m.
//Найти значение функции Аккермана для n = 1, m = 3.

// прим. Вид функции я взял с википедии, т.к. там рядом таблица значений + она немного отличается от функции данной в
//сборнике задач, соотв. думаю в сборнике - ошибка

import java.util.Scanner;

public class MainApp33 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите m: ");
        int m = sc.nextInt();
        System.out.print("Введите n: ");
        int n = sc.nextInt();

        Akkerman akkerman = new Akkerman();
        System.out.println("Значение функции равно " + akkerman.func(m, n));
    }
}

class Akkerman {
    public int func (int m, int n) {
        int result;
        if (m == 0) return n + 1;
        if (m > 0 & n == 0) {
            result = func(m - 1, 1);
            return result;
        }
        if (m > 0 & n > 0) {
            result = func(m - 1, func(m, n - 1));
            return result;
        }

        return 0;
    }

}