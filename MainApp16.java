//6.8. Дано число n. Из чисел 1, 4, 9, 16, 25, ... 
// напечатать те, которые не превышают n.


import java.util.ArrayList;
import java.util.Scanner;

public class MainApp16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число n: ");
        int n = sc.nextInt();

        ArrayList<Integer> list = new ArrayList<>(); //создаем массив-список из n элементов
        for (int i = 1; i <= n; i++) {
            list.add(i * i); //заполняем список квадратами чисел от 1 до n
        }

        System.out.println("Числа не превышающие " + n + ":");
        //просмотрим весь список и отберем только те элементы, что не превышают n
        for (Integer i : list) {
            if (i <= n) {
                System.out.print(i + " ");
            }
        }

    }
}