//10.56.* Написать рекурсивную функцию, определяющую, является ли заданное натуральное число простым

import java.util.Scanner;

public class MainApp37 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число: ");
        int n = sc.nextInt();

        Number number = new Number();
        System.out.println("Число является простым: " + number.simple(n));
    }
}

class Number {
    public boolean simple (int n) {
        if (n < 4) return true;
        int a = n / 2;
        if (n % a == 0) return false;

        simple(n-1);
        return true;
    }
}