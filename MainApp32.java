//10.49. Написать рекурсивную функцию для вычисления индекса максимального элемента массива из n элементов.

import java.util.Arrays;
import java.util.Scanner;

public class MainApp32 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите количество элементов массива: ");
        int n = sc.nextInt(); // количество элементов массива
        int[] arr = new int[n]; // создаем массив из n элементов

        // заполняем массив рандомными значениями
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
        }

        System.out.println("Массив: " + Arrays.toString(arr));
        MFunction mf = new MFunction();
        System.out.println("Индекс максимального элемента: " + mf.maxIndex(arr));
    }
}

class MFunction {

    // поиск наибольшего элемента в массиве
    private boolean isMaxElement(int[] arr, int element) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > element)
                return false;
        }
        return true;
    }

    // поиск индекса наибольшего элемента
    private int maxIndex(int[] arr, int start) {
        if (isMaxElement(arr, arr[start]))
            return start;
        return maxIndex(arr, start + 1);
    }

    // финальный метод
    public int maxIndex(int[] arr) {
        return maxIndex(arr, 0);
    }
}