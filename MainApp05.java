// 2.43 Даны два целых числа a и b. Если a делится на b или b делится на a, то вывести 1,
// иначе — любое другое число. Условные операторы и операторы цикла не использовать.

import java.util.Scanner;

public class MainApp05 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); // создаем объект Scanner
        System.out.println("Введите числа a и b: ");
        int a = sc.nextInt(); // вводим а
        int b = sc.nextInt(); // вводим b
        int c1 = a % b; // проверяем делится ли а на b
        int c2 = b % a; // проверяем делится ли b на a

        System.out.println("Результат задачи: " + (c1 * c2 + 1) ); // если а делится на b или b делится на а,
                                                                    // то получится 0 + 1, иначе любое другое число
    }
}