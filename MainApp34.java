// 10.51. Определить результат выполнения следующих рекурсивных процедур при n = 5

public class MainApp34 {
    public static void main(String[] args) {
        int n = 5;
        Task1051 task = new Task1051();
        System.out.println("Task A:");
        task.taskA(n);
        System.out.println("Task B:");
        task.taskB(n);
        System.out.println("Task C:");
        task.taskC(n);
    }
}

class Task1051 {
    public void taskA (int n) {
        if (n > 0) {
            System.out.println(n);
            taskA(n - 1);
        }
    }

    public void taskB (int n) {
        if (n > 0) {
            taskB(n - 1);
            System.out.println(n);
        }
    }

    public void taskC (int n) {
        if (n > 0) {
            System.out.println(n);
            taskC(n - 1);
            System.out.println(n);
        }
    }
}