// 5.64. В области 12 районов. Известны количество жителей (в тысячах человек) и площадь (в км2)
//каждого района. Определить среднюю плотность населения по области в целом.

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Arrays;

public class MainApp15 {
    public static void main(String[] args) {
        int a = 12; // количество районов

        double[] people = new double[a]; // массив с населением
        for (int i = 0; i < people.length; i++) {
            people[i] = Math.random() * 20;
        }

        double[] area = new double[a]; // массив с площадью
        for (int i = 0; i < area.length; i++) {
            area[i] = (Math.random() * 20) + 1; // прибавляем 1, чтобы не получился 0
        }
        DecimalFormat fm = new DecimalFormat("#0.000");

        System.out.println("Число жителей, тыс:");
        for (double i : people) {
            System.out.print(fm.format(i) + "\t");
        }
        System.out.println();
        System.out.println("Площадь района, км2:");
        for (double i : area) {
            System.out.print(fm.format(i) + "\t");
        }
        System.out.println();

        System.out.println("Плотность по районам: \n" + Arrays.toString(District.getPopulationDensity(people, area)));

        // форматируем вывод значения
        BigDecimal averageDensity = new BigDecimal(District.getAverageDensity(District.getPopulationDensity(people, area)));
        averageDensity = averageDensity.setScale(3, BigDecimal.ROUND_HALF_UP); // 3 - количество знаков после запятой

        System.out.println("Средняя плотность по области: " + averageDensity);
    }
}

class District {
    public static double[]  getPopulationDensity(double[] array, double[] array2) {
        double[] density = new double[array.length];
        for (int i = 0; i < density.length; i++) {
            density[i] = array[i] / array2[i];
        }
        return density;
    }

    public static String getAverageDensity(double[] array) {
        double average = 0;
        double sum = 0;
        for (int i = 0; i < array.length; i++) {
            sum = sum + array[i];
        }
        average = sum / array.length;
        String stringValue = Double.toString(average);

        return stringValue;
    }
}