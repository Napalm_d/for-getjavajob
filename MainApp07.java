//2.39 Даны целые числа h, m, s (0 < h ≤ 23, 0 ≤ m ≤ 59, 0 ≤ s ≤ 59), указывающие
// момент времени: "h часов, m минут, s секунд". Определить угол (в градусах)
// между положением часовой стрелки в начале суток и в указанный момент времени.

import java.util.Scanner;

public class MainApp07 {

    public static int degrees(int hours, int minutes, int seconds) {
       // за сутки часовая стрелка делает 2 оборота, второй оборот, если он есть, отбрасываем
       if (hours >= 12) {
           hours = hours - 12;
       }
       int result; // количество градусов
       result = ((hours * 3600) + (minutes * 60) + seconds) / 120; // считаем общее кол-во секунд, часовая стрелка делает 1 градус за 120 сек

       return result;
    }

    public static void main(String[] args) {
        int h; // часы, 0 <= h <= 23
        int m; // минуты, 0 <= m <= 59
        int s; // секунды, 0 <= m <= 59

        Scanner sc = new Scanner(System.in); // создаем объект Scanner для ввода с клавиатуры
        System.out.println("Введите часы h: ");
        h = sc.nextInt();
        System.out.println("Введите минуты m: ");
        m = sc.nextInt();
        System.out.println("Введите секунды s: ");
        s = sc.nextInt();

        System.out.println("Угол (в градусах) между положением часовой стрелки в начале суток и в указанный момент времени: " + degrees(h, m, s));

    }
}