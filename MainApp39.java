// 10.55.* Написать рекурсивную процедуру перевода натурального числа из десятичной системы счисления в N-ричную
// Значение N в основной программе вводится с клавиатуры (2 <= N <= 16).

import java.util.Scanner;

public class MainApp39 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        System.out.print("Введите натуральное десятичное число: ");
        int source = s.nextInt();
        System.out.print("Введите основание (2 <= N <= 16): ");
        int n = s.nextInt();

        Convert cnvrt = new Convert();
        System.out.print("Результат: ");
        System.out.println(cnvrt.converter(source, n, ""));
    }
}
class Convert{

    public String converter(int num, int n, String result) {
        char charMap[] = {'0', '1', '2', '3', '4', '5', '6',
                '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        int r = num % n;
        int rest = num / n;

        if (rest > 0) {
            return converter(rest, n, charMap[r] + result);
        }
        return charMap[r] + result;
    }
}