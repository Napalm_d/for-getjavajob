//4.106. Составить программу, которая в зависимости от порядкового номера дня месяца (1, 2, ..., 12)
//выводит на экран время года, к которому относится этот месяц.


public class MainApp12 {
    public static void main(String[] args) {
        Season.getSeason(2);
        Season.getSeason(5);
        Season.getSeason(9);
        Season.getSeason(7);
    }
}

class Season {
    public static void getSeason(int month) {
        switch (month) {
            case 1 :
            case 2 :
            case 12 :
                System.out.println("Winter");
                break;
            case 3 :
            case 4 :
            case 5 :
                System.out.println("Spring");
                break;
            case 6 :
            case 7 :
            case 8 :
                System.out.println("Summer");
                break;
            case 9 :
            case 10 :
            case 11 :
                System.out.println("Autumn");
                break;
        }
    }
}