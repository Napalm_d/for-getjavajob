// 12.24. Заполнить массив размером 6x6 так, как показано на рис. 12.2.

import java.util.Scanner;

public class MainApp43 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите размерность массива: ");
        int n = sc.nextInt();
        int[][] arrayA = new int[n][n];
        int[][] arrayB = new int[n][n];
        ArrayMod am = new ArrayMod();

        System.out.println("Массив А: ");
        am.taskA(arrayA, n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arrayA[i][j] + " ");
            }
            System.out.println("");
        }
        System.out.println("Массив B: ");
        am.taskB(arrayB, n);
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(arrayB[i][j] + " ");
            }
            System.out.println("");
        }
    }
}

class ArrayMod {
    public int[][] taskA (int[][] arr, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 | j == 0) {
                    arr[i][j] = 1;
                }
                if (i > 0 & j > 0) {
                    arr[i][j] = arr[i][j-1] + arr[i-1][j];
                }
            }
        }
        return arr;
    }

    public int[][] taskB (int[][] arr, int n) {
        int temp;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if ((i + j) < n) temp = 0;
                else temp = -n;
                arr[i][j] = (1 + i + j + temp) ;
            }
        }
        return arr;
    }
}