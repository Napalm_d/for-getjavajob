//10.53. Написать рекурсивную процедуру для ввода с клавиатуры последовательности
// чисел и вывода ее на экран в обратном порядке (окончание последовательности — при вводе нуля).

import java.util.Scanner;

public class MainApp36 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите количество элементов массива: ");
        int n = sc.nextInt(); // количество элементов массива
        int[] arr = new int[n]; // создаем массив из n элементов

        // заполняем массив рандомными значениями
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) (Math.random() * 100);
        }
        System.out.print("Массив: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i]);
        }

        ArrayReverse rv = new ArrayReverse();
        System.out.print("\nМассив в обратном порядке: ");

        for (int i = arr.length - 1; i > -1; i--) {
            rv.digitReverse(arr[i]);
        }
    }
}
class ArrayReverse {
    public void digitReverse (int n) {
        if (n < 10) System.out.print(n);
        if (n >= 10) {
            System.out.print(n % 10);
            digitReverse(n / 10);
        }
    }
}