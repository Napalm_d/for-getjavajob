//9.185.*Строка содержит арифметическое выражение, в котором используются круглые скобки, в том числе вложенные.
// Проверить, правильно ли в нем расставлены скобки.
// а) Ответом должны служить слова да или нет.
// б) В случае неправильности расстановки скобок:
//  если имеются лишние правые (закрывающие) скобки, то выдать сообщение с указанием позиции первой такой скобки;
//  если имеются лишние левые (открывающие) скобки, то выдать сообщение с указанием количества таких скобок.
// Если скобки расставлены правильно, то сообщить об этом.

public class MainApp49a {

    public static void main(String[] args) {

        String example = "44 * (55 - 1) + ((12 + x ) / (54 + 1)) * 2";
        String example2 = "25 + (((5 - 3(-9 + (25))";
        String example3 = ")45 + )38 * (54 - 24/2)";

        Brackets br = new Brackets();
        System.out.println(example);
        br.isCorrectBrackets(example.toCharArray());
        System.out.println(example2);
        br.isCorrectBrackets(example2.toCharArray());
        System.out.println(example3);
        br.isCorrectBrackets(example3.toCharArray());

    }
}

class Brackets {

    public void isCorrectBrackets (char[] arr) {
        int k = 0; // счетчик открывающихся скобок
        for (int i = 0; i < arr.length; i++) {
            switch (arr[i]) {
                case '(' : k++;
                    break;
                case ')' : if (k > 0) k--;
                else System.out.println("Нет. Лишняя закрывающая скобка в позиции: " + (i + 1));
                    break;
            }
        }
        if (k > 0) System.out.println("Нет. Лишние открывающие скобки. Их количество: " + k);
        else System.out.println("Да.");
    }
}