// OOP task

import java.util.ArrayList;

public class OOPtask {
    public static void main(String[] args) {

        Interview interview = new Interview();
        ArrayList<Candidate> listOfSeekers = new ArrayList<>(); // список кандидатов

        Employer employer = new Employer();

        Candidate candidate1 = new SLCandidate("Adam");
        Candidate candidate2 = new GJJCandidate("Bob");
        Candidate candidate3 = new GJJCandidate("Matt");
        Candidate candidate4 = new SLCandidate("Dave");
        Candidate candidate5 = new GJJCandidate("Marta");
        Candidate candidate6 = new GJJCandidate("Scott");
        Candidate candidate7 = new SLCandidate("John");
        Candidate candidate8 = new GJJCandidate("Jesus");
        Candidate candidate9 = new SLCandidate("Linda");
        Candidate candidate10 = new SLCandidate("Nick");

        // добавляем каждого кандидата в список
        interview.addToList(listOfSeekers, candidate1);
        interview.addToList(listOfSeekers, candidate2);
        interview.addToList(listOfSeekers, candidate3);
        interview.addToList(listOfSeekers, candidate4);
        interview.addToList(listOfSeekers, candidate5);
        interview.addToList(listOfSeekers, candidate6);
        interview.addToList(listOfSeekers, candidate7);
        interview.addToList(listOfSeekers, candidate8);
        interview.addToList(listOfSeekers, candidate9);
        interview.addToList(listOfSeekers, candidate10);

        // проходимся по списку и вызываем startInterview для каждого члена списка
        for (Candidate candidate : listOfSeekers) {
            interview.startInterview(employer, candidate);
        }
            }
}

class Interview {

    public ArrayList addToList(ArrayList list, Candidate candidate) {
        list.add(candidate);
        return list;
    }

    public void startInterview (Employer employer, Candidate candidate) {
        employer.greeting();
        candidate.greeting();
        candidate.javaExperience();
    }
}

interface JavaLearner {

    void javaExperience();

}

abstract class JavaSpecialist {
    String name;

    JavaSpecialist() { this.name = null; }
    JavaSpecialist(String name) {this.name = name;}

    public abstract void greeting();
}

class Employer extends JavaSpecialist {

    @Override
    public void greeting() {
        System.out.println("Interviewer: Hi! Introduce yourself and describe your Java experience, please.");
    }
}

class Candidate extends JavaSpecialist implements JavaLearner {

    Candidate(String name) { super(name); }

    @Override
    public void greeting() {
        System.out.println("Candidate: Hi! My name is " + this.name + ".");
    }

    @Override
    public void javaExperience() {
        System.out.println("Candidate: Unfortunately, I have no Java experience.\n");
    }
}

class SLCandidate extends Candidate {

    SLCandidate(String name) { super(name); }

    @Override
    public void javaExperience() {
        System.out.println("Candidate: I have been learning Java by myself, nobody examined how through is my knowledge and how good is my code.\n");
    }
}

class GJJCandidate extends Candidate {

    GJJCandidate(String name) { super(name); }

    @Override
    public void javaExperience() {
        System.out.println("Candidate: I passed successfully getJavaJob exams and code reviews.\n");
    }
}