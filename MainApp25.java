//10.42. В некоторых языках программирования (например, в Паскале) не предусмотрена операция возведения в степень.
// Написать рекурсивную функцию для расчета степени n вещественного числа a (n — натуральное число).

import java.util.Scanner;

public class MainApp25 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число a и его степень n");
        System.out.print("a: ");
        double numberA = sc.nextDouble();
        System.out.print("n: ");
        int numberN = sc.nextInt();

        MathFunction mf = new MathFunction();
        System.out.println("Ответ: " + mf.power(numberA, numberN));
    }
}

class MathFunction {

    public double power (double a, int n) {
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return a;
        }

        if (a == 1) {
            return 1;
        }

        if (n < 0) {
            return 1 / (a * power(a, n + 1));
        }
        // оставшееся условие n > 1, не стал прятать под if
        return a * power(a, n - 1);
    }
}