// 12.28.*Заполнить двумерный массив размером 5x5 так, как представлено на рис. 12.4.

import java.util.Scanner;

public class MainApp38 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите размерность массива: ");
        int n = sc.nextInt();

        int A[][] = new int[n][n];
        int k = 1; // значение элемента массива, от 1 до n * n
        int c1 = 0; // индекс столбца, откуда начнется заполнение
        int c2 = n - 1; // индекс столбца, где мы остановимся
        int r1 = 0; // индекс строки, с которой начинается заполнение
        int r2 = n - 1; // индекс строки, где мы остановимся

        while (k <= n * n) {
            // заполнение верхней строки
            for (int i = c1; i <= c2; i++) {
                A[r1][i] = k;
                k++;
            }
            // заполнение правого столбца
            for (int j = r1 + 1; j <= r2; j++) {
                A[j][c2] = k;
                k++;
            }
            // заполнение нижней строки
            for (int i = c2 - 1; i >= c1; i--) {
                A[r2][i] = k;
                k++;
            }
            // заполнение правого столбца
            for (int j = r2 - 1; j >= r1+1; j--) {
                A[j][c1] = k;
                k++;
            }
            // сужение диапазонов
            c1++;
            c2--;
            r1++;
            r2--;
        }

        System.out.println("Заполненный массив:");
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(A[i][j] + "\t");
            }
            System.out.println();
        }
    }
}