//10.46. Даны первый член и знаменатель геометрической прогрессии. Написать рекурсивную функцию:
//а) нахождения n-го члена прогрессии;
//б) нахождения суммы n первых членов прогрессии.


import java.util.Scanner;

public class MainApp29 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите первый член прогрессии: ");
        double a = sc.nextInt(); // первый член прогрессии
        System.out.print("Введите знаменатель прогрессии: ");
        double q = sc.nextInt(); // знаменатель прогрессии
        System.out.print("Введите n: ");
        int n = sc.nextInt();
        
        MathF mf = new MathF();
        System.out.println(n + "-й член прогрессии: " + mf.nProgMember(a, q, n));
        System.out.println("Сумма " + n + " первых членов: " + mf.sumProg(a, q, n));
    }
}

class MathF {
    public double nProgMember(double a, double q, int n) {
        if (n == 1) return a;
        double result = nProgMember(a, q, n - 1) * q;
        return result;
    }

    public double sumProg(double a, double q, int n) {
        if (n == 1) return a;
        double result = sumProg(a, q, n - 1) + nProgMember(a, q, n);
        return result;
    }
}