//9.15. Дано слово. Вывести на экран его k-й символ.

import java.util.Scanner;

public class MainApp18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите слово: ");
        String word = sc.next();

        System.out.print("Введите k: ");
        int k = sc.nextInt();

        System.out.println(k + "-й символ введенного слова: " + word.charAt(k - 1)); // Отнимаем 1, т.к. индексы символов считаются с 0
    }
}