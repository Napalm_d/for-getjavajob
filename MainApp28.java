//10.45. Даны первый член и разность арифметической прогрессии. Написать рекурсивную функцию для нахождения:
//а) n-го члена прогрессии;
//б) суммы n первых членов прогрессии.

import java.util.Scanner;

public class MainApp28 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите первый член прогрессии: ");
        double a = sc.nextInt(); // первый член прогрессии
        System.out.print("Введите разность прогрессии: ");
        double d = sc.nextInt(); // разность прогрессии
        System.out.print("Введите n: ");
        int n = sc.nextInt();

        MathFunc mf = new MathFunc();
        System.out.println(n + "-й член прогрессии: " + mf.nProgMember(a, d, n));
        System.out.println("Сумма " + n + " первых членов: " + mf.sumProg(a, d, n));
    }
}

class MathFunc {

    public double nProgMember(double a, double d, int n) {
        if (n == 1) return a;
        double result = nProgMember(a, d, n - 1) + d;
        return result;
    }

    public double sumProg (double a, double d, int n) {
        if (n == 1) return a;
        double result = sumProg(a, d, n-1) + nProgMember(a, d, n);
        return result;
    }
}