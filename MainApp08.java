//4.15. Известны год и номер месяца рождения человека, а также год и номер месяца
// сегодняшнего дня (январь — 1 и т. д.). Определить возраст человека (число полных лет).
// В случае совпадения указанных номеров месяцев считать, что прошел полный год.

public class MainApp08 {
    public static void main(String[] args) {
        Counter.ageCounter(1985, 6, 2014, 12); //test1
        Counter.ageCounter(1985, 6, 2014, 5); //test2
        Counter.ageCounter(1985, 6, 2014, 6); //test3
    }
}

class Counter {
    public static void ageCounter(int yearBirth, int monthBirth, int yearNow, int monthNow) {
        int age = yearNow - yearBirth;
        if (monthNow < monthBirth) {
            age = age - 1;
        }
        System.out.println("Возраст равен " + age);
    }
}