//5.10. Напечатать таблицу перевода 1, 2, ... 20 долларов США в рубли по текущему курсу
// (значение курса вводится с клавиатуры).


import java.util.Arrays;
import java.util.Scanner;

public class MainApp13 {
    public static void main(String[] args) {
        double[][] money = new double[2][20]; // создаем массив, 1 строка - доллары, 2 строка - рубли

        //заполняем массив
        for (int j = 0; j < 20; j++) {
            money[0][j] = j + 1;
            money[1][j] = 0;
        }
        // создаем Scanner для ввода с клавиатуры
        Scanner sc = new Scanner(System.in);

        System.out.print("Введите курс доллара: ");
        double course = sc.nextDouble();

        for (int j = 0; j < 20; j++) {
           money[1][j] = money[0][j] * course;
        }
        //System.out.println(Arrays.deepToString(money)); //один из вариантов вывода массива

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 20; j++) {
                System.out.print(money[i][j] + " ");
            }
            System.out.println("");
        }
    }
}