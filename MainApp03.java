// 2.13. Дано трехзначное число. Найти число, полученное при прочтении его цифр справа налево.
// 200 > n > 100

import java.util.Scanner;

public class MainApp03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in); // создаем объект Scanner
        System.out.print("Введите число от 100 до 200: ");
        int n = sc.nextInt(); // вводим число n с клавиатуры
        int mirror = 0; // переменная для отзеркалиного числа
        while (n != 0) {
            mirror = mirror * 10 + n % 10;
            n = n / 10;
        }
        System.out.println("Число n после преобразования: " + mirror);
    }
}
