//9.166. Дано предложение. Поменять местами его первое и последнее слово.

public class MainApp23 {
    public static void main(String[] args) {
        String sentence = "One two three four five six seveN";
        String[] str = sentence.split(" ");

        // выдергиваем первое и последнее слова
        String firstWord = str[0];
        String lastWord = str[str.length-1];
        String temp = firstWord;

        // меняем слова местами
        str[0] = lastWord;
        str[str.length-1] = temp;

        String newSentence = "";
        // собираем преобразованную строку
        for (int i = 0; i < str.length; i++) {
            newSentence = newSentence + str[i] + " ";
        }

        System.out.println(newSentence);
    }
}