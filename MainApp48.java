// 12.25. Заполнить двумерный массив так, как представлено на рис. 12.3.

public class MainApp48 {
    public static void main(String[] args) {

        // ****TASK A****
        int[][] arrA = new int[12][10];
        System.out.println("a)");
        int n = 1;
        for (int i = 0; i < arrA.length; i++) {
            for (int j = 0; j < arrA[0].length; j++) {
                arrA[i][j] = n;
                n++;
            }
        }
        printArray(arrA); // print task a array

        // ****TASK B****
        int[][] arrB = new int[12][10];
        System.out.println("b)");
        n = 1;
        for (int j = 0; j < arrB[0].length; j++) {
            for (int i = 0; i < arrB.length; i++) {
                arrB[i][j] = n;
                n++;
            }
        }
        printArray(arrB); // print task b array

        // ****TASK V****
        int[][] arrV = new int[12][10];
        System.out.println("v)");
        n = 1;
        for (int i = 0; i < arrV.length; i++) {
            for (int j = arrV[0].length - 1; j >= 0; j--) {
                arrV[i][j] = n;
                n++;
            }
        }
        printArray(arrV); // print task v array

        // ****TASK G****
        int[][] arrG = new int[12][10];
        System.out.println("g)");
        n = 1;
        for (int j = 0; j < arrG[0].length; j++) {
            for (int i = arrG.length - 1; i >= 0; i--) {
                arrG[i][j] = n;
                n++;
            }
        }
        printArray(arrG); // print task g array

        // ****TASK D****
        int[][] arrD = new int[10][12];
        System.out.println("d)");
        n = 1;
        for (int i = 0; i < arrD.length; i++) {
            if (i % 2 == 0) {
                for (int j = 0; j < arrD[0].length; j++) {
                    arrD[i][j] = n;
                    n++;
                }
            } else {
                for (int j = arrD[0].length - 1; j >= 0; j--) {
                    arrD[i][j] = n;
                    n++;
                }
            }
        }
        printArray(arrD); // print task d array

        // ****TASK E****
        int[][] arrE = new int[12][10];
        System.out.println("e)");
        n = 1;
        for (int j = 0; j < arrE[0].length; j++) {
            if (j % 2 == 0) {
                for (int i = 0; i < arrE.length; i++) {
                    arrE[i][j] = n;
                    n++;
                }
            } else {
                for (int i = arrE.length - 1; i >= 0; i--) {
                    arrE[i][j] = n;
                    n++;
                }
            }
        }
        printArray(arrE); // print task e array

        // ****TASK Zh****
        int[][] arrZh = new int[12][10];
        System.out.println("zh)");
        n = 1;
        for (int i = arrZh.length - 1; i >= 0; i--) {
            for (int j = 0; j < arrZh[0].length; j++) {
                arrZh[i][j] = n;
                n++;
            }
        }
        printArray(arrZh); // print task zh array

        // ****TASK Z****
        int[][] arrZ = new int[12][10];
        System.out.println("z)");
        n = 1;
        for (int j = arrZ[0].length - 1; j >= 0; j--) {
            for (int i = 0; i < arrZ.length; i++) {
                arrZ[i][j] = n;
                n++;
            }
        }
        printArray(arrZ); // print task z array

        // TASK I
        int[][] arrI = new int[12][10];
        System.out.println("i)");
        n = 1;
        for (int i = arrI.length - 1; i >= 0; i--) {
            for (int j = arrI[0].length - 1; j >= 0; j--) {
                arrI[i][j] = n;
                n++;
            }
        }
        printArray(arrI); // print task i array

        // ****TASK K****
        int[][] arrK = new int[12][10];
        System.out.println("k)");
        n = 1;
        for (int j = arrK[0].length - 1; j >= 0; j--) {
            for (int i = arrK.length - 1; i >= 0; i--) {
                arrK[i][j] = n;
                n++;
            }
        }
        printArray(arrK); // print task k array

        // ****TASK L****
        int[][] arrL = new int[12][10];
        System.out.println("l)");
        n = 1;
        for (int i = arrL.length - 1; i >= 0; i--) {
            if (i % 2 == 1) {
                for (int j = 0; j < arrL[0].length; j++) {
                    arrL[i][j] = n;
                    n++;
                }
            } else {
                for (int j = arrL[0].length - 1; j >= 0; j--) {
                    arrL[i][j] = n;
                    n++;
                }
            }
        }
        printArray(arrL); // print task l array

        // ****TASK M****
        int[][] arrM = new int[12][10];
        System.out.println("m)");
        n = 1;
        for (int i = 0; i < arrD.length; i++) {
            if (i % 2 == 0) {
                for (int j = arrM[0].length - 1; j >= 0; j--) {
                    arrM[i][j] = n;
                    n++;
                }
            } else {
                for (int j = 0; j < arrM[0].length; j++) {
                    arrM[i][j] = n;
                    n++;
                }
            }
        }
        printArray(arrM); // print task m array

        // ****TASK N****
        int[][] arrN = new int[12][10];
        System.out.println("n)");
        n = 1;
        for (int j = arrN[0].length - 1; j >= 0; j--) {
            if (j % 2 == 1) {
                for (int i = 0; i < arrN.length; i++) {
                    arrN[i][j] = n;
                    n++;
                }
            } else {
                for (int i = arrN.length - 1; i >= 0; i--) {
                    arrN[i][j] = n;
                    n++;
                }
            }
        }
        printArray(arrN); // print task n array

        // ****TASK O****
        int[][] arrO = new int[12][10];
        System.out.println("o)");
        n = 1;
        for (int j = 0; j < arrO[0].length; j++) {
            if (j % 2 == 0) {
                for (int i = arrN.length - 1; i >= 0; i--) {
                    arrN[i][j] = n;
                    n++;
                }
            } else {
                for (int i = 0; i < arrN.length; i++) {
                    arrN[i][j] = n;
                    n++;
                }
            }
        }
        printArray(arrN); // print task o array

        // ****TASK P****
        int[][] arrP = new int[12][10];
        System.out.println("p)");
        n = 1;
        for (int i = arrP.length - 1; i >= 0; i--) {
            if (i % 2 == 1) {
                for (int j = arrP[0].length - 1; j >= 0; j--) {
                    arrP[i][j] = n;
                    n++;
                }
            } else {
                for (int j = 0; j < arrP[0].length; j++) {
                    arrP[i][j] = n;
                    n++;
                }
            }
        }
        printArray(arrP);

        // ****TASK R****
        int[][] arrR = new int[12][10];
        System.out.println("r)");
        n = 1;
        for (int j = arrR[0].length - 1; j >= 0; j--) {
            if (j % 2 == 1) {
                for (int i = arrR.length - 1; i >= 0; i--) {
                    arrR[i][j] = n;
                    n++;
                }
            } else {
                for (int i = 0; i < arrR.length; i++) {
                    arrR[i][j] = n;
                    n++;
                }
            }
        }
        printArray(arrR); // print task r array
    }


    public static void printArray (int[][] arr) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                System.out.format("%4d", arr[i][j]);
            }
            System.out.println("");
        }
    }
}