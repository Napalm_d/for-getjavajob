//6.87. Составить программу, которая ведет учет очков, набранных каждой командой
// при игре в баскетбол. Количество очков, полученных командами в ходе игры,
// может быть равно 1, 2 или 3. После любого изменения счет выводить на экран.
// После окончания игры выдать итоговое сообщение и указать номер команды-победительницы.
// Окончание игры условно моделировать вводом количества очков, равного нулю.

import java.util.Scanner;

public class MainApp17 {
    public static void main(String[] args) throws Exception {
        Game game = new Game();
        game.play();
    }
}

class Game {
    private static int score1 = 0;
    private static int score2 = 0;
    private static String team1name;
    private static String team2name;

    void play() throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter team #1: ");
        team1name = sc.next();

        System.out.print("Enter team #2: ");
        team2name = sc.next();

        score();
    }

    String score() {
       int scoreChange = 0;
       int choice;
       boolean gameContinue = true;
       Scanner sc = new Scanner(System.in);

       while (gameContinue) {
       System.out.println("Enter team to score (1 or 2 or 0 to finish the game): ");
       choice = sc.nextInt();
            switch (choice) {
                case 1:
                    System.out.println("Enter score (1 or 2 or 3): ");
                    scoreChange = sc.nextInt();
                    score1 = score1 + scoreChange;
                    break;
                case 2:
                    System.out.println("Enter score (1 or 2 or 3): ");
                    scoreChange = sc.nextInt();
                    score2 = score2 + scoreChange;
                    break;
                case 0:
                    gameContinue = false;
                    result();
            }
       }
       return "The match is over";
    }

    String result() {
       System.out.println("The match is over");
       System.out.println("The final score is:");
       System.out.println(team1name + " " + score1 + ":" + score2 + " " + team2name);
       if (score1 > score2) {
           System.out.println(team1name + " WINS");
       } else if (score1 < score2) {
           System.out.println(team2name + " WINS");
       } else System.out.println("DRAW");

       return "Goodbye!";
    }
}