//4.33. Дано натуральное число.
// а) Верно ли, что оно заканчивается четной цифрой?
// б) Верно ли, что оно заканчивается нечетной цифрой?
// Примечание
// В обеих задачах составное условие не использовать.


import java.util.Scanner;

public class MainApp09 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите число: ");
        int a = sc.nextInt();
        if (a % 2 == 0) {
            System.out.println("Число заканчивается четной цифрой");
        } else {
            System.out.println("Число заканчивается нечетной цифрой");
        }
    }
}